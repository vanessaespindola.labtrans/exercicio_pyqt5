# -*- coding: utf-8 -*-

import sys, os
import PyQt5
from PyQt5 import uic, QtWidgets

'''
dirname = os.path.dirname(PyQt5.__file__)
plugin_path = os.path.join(dirname, 'plugins', 'platforms')
'''
os.environ['QT_QPA_PLATFORM_PLUGIN_PATH'] = "C:\\Users\\Cecília\\AppData\\Local\\Programs\\Python\\Python310\\Lib\\site-packages\\PyQt5\\Qt5\\plugins\\platforms"

def chama_segunda_tela():
	segunda_tela.show()
	segunda_tela.label.setText("Olá mundo")


app = QtWidgets.QApplication(sys.argv)
primeira_tela=uic.loadUi("primeira_tela.ui")
segunda_tela=uic.loadUi("segunda_tela.ui")
primeira_tela.pushButton.clicked.connect(chama_segunda_tela)

primeira_tela.show()
app.exec()
