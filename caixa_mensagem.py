from PyQt5 import uic, QtWidgets
from PyQt5.QtWidgets import QMessageBox


def Exibir_mensagem():
	dado_lido=janela.lineEdit.text()
	print(dado_lido)
	janela.lineEdit.setText("")
	QMessageBox.about(janela, "Alerta", dado_lido)
	if dado_lido =="":
		QMessageBox.about(janela, "Alerta", "Nenhum nome digitado")
	else:
		QMessageBox.about(janela, "Alerta", "Olá : "+dado_lido)

app=QtWidgets.QApplication([])
janela=uic.loadUi("caixa_mensagem.ui")
janela.pushButton.clicked.connect(Exibir_mensagem)

janela.show()
app.exec()