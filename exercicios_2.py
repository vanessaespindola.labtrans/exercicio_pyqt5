import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QToolTip, QLabel, QLineEdit
from PyQt5 import QtGui

class Janela (QMainWindow):
	def __init__(self):
		super().__init__()

		self.topo = 100
		self.esquerda = 100
		self.largura = 900
		self.altura = 600
		self.titulo = "Primeira Janela"

		self.caixa_texto = QLineEdit(self)
		self.caixa_texto.move(20,25)
		self.caixa_texto.resize(220,40)


		botao1 = QPushButton("Botao 1", self)
		botao1.move(250,200)
		botao1.resize(150,80);
		botao1.setStyleSheet("QPushButton {background-color:#0FB600; font:bold; font-size:20 px}")
		botao1.clicked.connect(self.botao1_click)

		botao_caixa = QPushButton("Enviar texto", self)
		botao_caixa.move (650,200)
		botao_caixa.resize(150,80)
		botao_caixa.setStyleSheet("QPushButton {background-color:#0FB600; font:bold; font-size:20 px}")
		botao_caixa.clicked.connect(self.mostra_texto)


		botao2 = QPushButton("Botao 2", self)
		botao2.move(450,200)
		botao2.resize(150,80);
		botao2.setStyleSheet("QPushButton {background-color:#FFCCE5; font:bold; font-size:20 px;}")
		botao2.clicked.connect(self.botao2_click)
		self.label_1 = QLabel(self)
		self.label_1.setText("Aperte algum botão")
		self.label_1.move(100,150)
		self.label_1.setStyleSheet("QLabel {font:bold; font-size:30 px;  color: blue}")
		self.label_1.resize(400,25)

		self.label_caixa = QLabel(self)
		self.label_caixa.setText("Digitou: ")
		self.label_caixa.move(700,150)
		self.label_caixa.setStyleSheet("QLabel {font:bold; font-size:30 px;  color: blue}")
		self.label_caixa.resize(400,25)

		self.carro = QLabel(self)
		self.carro.move(100,300)
		self.carro.setPixmap(QtGui.QPixmap("carro-verde1.png"))
		self.carro.resize(600,200)
		self.CarregarJanela()

	def CarregarJanela(self):
		self.setGeometry (self.topo, self.esquerda, self.largura, self.altura)
		self.setWindowTitle(self.titulo)
		self.show()

	def botao1_click(self):
		self.label_1.setText("O botão 1 foi clicado")
		self.label_1.setStyleSheet("QLabel {font:bold; font-size:30 px;  color: green}")
		self.carro.setPixmap(QtGui.QPixmap("carro-verde1.png"))

	def botao2_click(self):
		self.label_1.setText("O botão 2 foi clicado")
		self.label_1.setStyleSheet("QLabel {font:bold; font-size:30 px;  color: black}")
		self.carro.setPixmap(QtGui.QPixmap("carro-rosa1.png"))

	def mostra_texto(self):
		conteudo = self.caixa_texto.text()

		self.label_caixa.setText("Digitou: " + conteudo)


aplicacao = QApplication(sys.argv)
j= Janela()
sys.exit(aplicacao.exec_())




