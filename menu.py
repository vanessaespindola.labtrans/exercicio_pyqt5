# -*- coding: utf-8 -*-

import bug
from PyQt5 import uic, QtWidgets

def menu_azul():
	tela.label_2.setText('Azul')
	tela.label_2.setStyleSheet("color: blue")

def menu_verde():
	tela.label_2.setText('Verde')
	tela.label_2.setStyleSheet("color: green")

def menu_amarelo():
	tela.label_2.setText('Amarelo')
	tela.label_2.setStyleSheet("color: yellow")

app = QtWidgets.QApplication([])
tela=uic.loadUi("menu.ui")

tela.actionAzul.triggered.connect(menu_azul)
tela.actionVerde.triggered.connect(menu_verde)
tela.actionAmarelo.triggered.connect(menu_amarelo)
tela.show()
app.exec()
 