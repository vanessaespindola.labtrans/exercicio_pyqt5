# -*- coding: utf-8 -*-

import sys
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QToolTip, QLabel, QLineEdit
from PyQt5 import QtGui


class Janela (QMainWindow):
	def __init__(self):
		super().__init__()

		self.ui = uic.loadUi("janela.ui", self)
		self.show()


app = QApplication(sys.argv)
win = Janela()
sys.exit(app.exec_())
