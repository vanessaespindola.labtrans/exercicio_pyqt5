# -*- coding: utf-8 -*-

import bug
from PyQt5 import uic, QtWidgets

def somar():
	soma = 0
	if(primeira_tela.checkBox_2.isChecked()):
		soma+= 15
		primeira_tela.checkBox_2.setChecked(False)
	if(primeira_tela.checkBox_3.isChecked()):
		soma+= 20
		primeira_tela.checkBox_3.setChecked(False)
	if(primeira_tela.checkBox_6.isChecked()):
		soma+= 15
		primeira_tela.checkBox_6.setChecked(False)
	if(primeira_tela.checkBox_2.isChecked()):
		soma+= 32
		primeira_tela.checkBox_2.setChecked(False)
	if(primeira_tela.checkBox_5.isChecked()):
		soma+= 5.50
		primeira_tela.checkBox_5.setChecked(False)

	primeira_tela.label.setText('Valor total:' + " " +str(soma))

app = QtWidgets.QApplication([])
primeira_tela=uic.loadUi("check_box.ui")
primeira_tela.pushButton.clicked.connect(somar)

primeira_tela.show()
app.exec()