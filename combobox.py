import bug
from PyQt5 import uic, QtWidgets

def Opcao_selecionada():
	cidade = tela.comboBox.currentText()
	tela.label_2.setText("Cidade: " + " " + cidade)

app = QtWidgets.QApplication([])
tela=uic.loadUi("combobox.ui")

tela.comboBox.addItems(["Florianópolis", "Imbituba", "Garopaba"])
tela.pushButton.clicked.connect(Opcao_selecionada)
tela.show()
app.exec()