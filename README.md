
# Exercício

## Objetivo
Criar em Python e PyQt um aplicativo simples, que tenha:

- um menu que abra uma nova janela
- campos de entrada de vários tipos (texto, combobox, checkbox)
- uma função para salvamento dos dados inseridos em um arquivo .txt
- uma função para abertura do arquivo txt e exibição do seu conteúdo.

## Estrutura de pastas e arquivos
```text
exercicio
├──
├── README.md
└── requirements.txt
```
## Requisitos
- Python
- Pip
- Virtualenv

## Instalação
1. [Clonar](https://gitlab.com/vanessaespindola.labtrans/exercicio_pyqt5)o repositório;

2. Crie um ambiente virtual
```sh
$ virtualenv venv
```
3. Ative o ambiente virtual:
```sh
$ exercicio/venv/scripts/activate
```
4. Instale as bibliotecas dependentes do projeto:
```sh
$ pip install -r requirements.txt
```

## Executar o programa
Execute o exercicio desejado apontando arquivo do exercicio para o python, ex: 
```sh
$ python menu.py
```
## Janela
[![](Tela_abrir_arquivo.jpg "Programa exercício")]()

## Referências
- [Python](https://www.python.org/)
- [Qt](https://doc.qt.io/qt-5.15/)
- [Git](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)
- [GitIgnore](https://docs.gitlab.com/ee/api/templates/gitignores.html)
- [Markdown](https://docs.gitlab.com/ee/user/markdown.html)
- [License](https://docs.gitlab.com/ee/user/compliance/license_compliance/)
