# -*- coding: utf-8 -*-

import bug
from PyQt5 import uic, QtWidgets

def Frame1():
	tela.frame_2.close()

def Frame2():
	tela.frame_2.show()

app = QtWidgets.QApplication([])
tela=uic.loadUi("frame.ui")

tela.pushButton_2.clicked.connect(Frame1)
tela.pushButton_3.clicked.connect(Frame2)
tela.show()
app.exec()